# Warning: This app is long, so for your case I recommend you to put the db and the User definition into anotehr file
# Also I recommend you to use templates, see 'app.py' for more informations
from flask import Flask, request
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from loginpy import *

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SECRET_KEY'] = 'aghzi vnguierhtrutizo hard to guess indeeed'
db = SQLAlchemy(app)

# Define the User database using login.py
User = initLogin(app, db)
db.create_all()

@app.route('/login')
def renderLogin():
    return """<form action='/login/post' method='post'><p>Username: <input type="text" name="username"></p><p>Password: <input type="password" name="password"></p><p><button type="submit">Login</button></p></form>"""

@app.route('/register')
def renderRegister():
    return """<form action='/register/post' method='post'><p>Username: <input type="text" name="username"></p><p>Password: <input type="password" name="password"></p><p><button type="submit">Register</button></p></form>"""
    
@app.route('/login/post', methods=['POST'])
def login():
    try:
        loginUser(request.form['username'], request.form['password'], User)
        return "You are now logged in as " + current_user.username
    except: return "Invalid username or password"

@app.route('/register/post', methods=['POST'])
def register():
    try:
        createUser(request.form['username'], request.form['password'], db, User)
        return "New user created you are now logged in as " + current_user.username
    except: return "This username is already taken"

@app.route('/logout') # Logout the current user is simpple
def logout(): 
    logout_user()
    return "You are now logged out."

@app.route('/locked')
@login_required # Using login_required to make a page private
def locked(): return "Hello " + current_user.username + " welcome to your private page."

# Run the app in mode debug
app.run(debug=True)
