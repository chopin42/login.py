# Login.py

Login.py is a simple library for Flask that makes you able to handle login easily without getting plenty of useless lines of code.

## Installation
### Quick installation

> This method doesn't install the depedencies

```bash
pip3 install git+https://gitea.com/chopin42/login.py
```

> I am not using the main PyPI website because the code don't work where it comes from the main PyPI, and also for flexibility reasons. 

### Full installation using PIP

```bash
sudo apt install python3 python3-pip
git clone https://gitea.com/chopin42/login.py && cd login.py
pip3 install -r requirements.txt && pip3 install .
```

### Full installation using Makefile

```bash
git clone https://gitea.com/chopin42/login.py && cd login.py
make debian # You can also run "make install" or "make fedora"
```

## Usage

You can find the full demo of login.py [here](./login-ex.py). You can also find the source code of login.py [here](./login.py/__init__.py).

To test the app you can run:

```bash
git clone https://gitea.com/chopin42/login.py && cd login.py
make install && make run
```

Then you can test the pages [register](http://localhost:5000/register), [login](http://localhost:5000/login), [locked](http://localhost:5000/locked) and [logout](http://localhost:5000/logout).

### initLogin(app, db)

This function is used to create the `User` class. Having this function is necessary for the rest of the program. Without this, the Users cannot get stored into the database.

```python
from flask import Flask, request
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from login import *

# Create the "app" and "db" variables
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SECRET_KEY'] = 'aghzi vnguierhtrutizo hard to guess indeeed'
db = SQLAlchemy(app)

# Create the User class and add it to the database
User = initLogin(app, db)
db.create_all()
```

### createUser(username, password, db, User)

This function is used to sign up a new user into the database. This is an example of usage:

```python
@app.route('/register/post', methods=['POST'])
def register():
    try:
        createUser(request.form['username'], request.form['password'], db, User)
        return "New user created you are now logged in as " + current_user.username
    except: 
    		return "This username is already taken"
```

### loginUser(username, password, User)

This function is used to login a user, that means that the user must already be in the database.

```python
@app.route('/login/post', methods=['POST'])
def login():
    try:
        loginUser(request.form['username'], request.form['password'], User)
        return "You are now logged in as " + current_user.username
    except: 
    		return "Invalid username or password"
```


### logout_user()

This is not a function from login.py, but a function from flask_login. The usage of this function is very simple:

```python
@app.route('/logout')
def logout(): 
    logout_user()
    return "You are now logged out."
```

### \@login_required

When you add this line under a url route, it locks the page to the logged in users. The users that are not logged will have a `Unauthorized` error showing up.

```python

@app.route('/locked')
@login_required # Using login_required to make a page private
def locked(): 
	return "Hello " + current_user.username + " welcome to your private page."
```