install:
	pip3 install -r requirements.txt --user 
	pip3 install .

debian:
	sudo apt -y install python3 python3-pip
	pip3 install -r requirements.txt --user
	pip3 install .

fedora:
	sudo dnf -y install python3 python3-pip
	pip3 install -r requirements.txt --user
	pip3 install .

run:
	python3 login-ex.py